use logos::Logos;

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq, Logos)]
#[repr(u16)]
pub enum SyntaxKind {
    #[regex(r"[ \n\r\t]+")]
    Whitespace = 0,
    #[regex(r"--[^\r\n]*")]
    Comment,

    #[token("and")]
    TokAnd,
    #[token("break")]
    TokBreak,
    #[token("do")]
    TokDo,
    #[token("else")]
    TokElse,
    #[token("elseif")]
    TokElseIf,
    #[token("end")]
    TokEnd,
    #[token("false")]
    TokFalse,
    #[token("for")]
    TokFor,
    #[token("function")]
    TokFunction,
    #[token("goto")]
    TokGoto,
    #[token("if")]
    TokIf,
    #[token("in")]
    TokIn,
    #[token("local")]
    TokLocal,
    #[token("nil")]
    TokNil,
    #[token("not")]
    TokNot,
    #[token("or")]
    TokOr,
    #[token("repeat")]
    TokRepeat,
    #[token("return")]
    TokReturn,
    #[token("then")]
    TokThen,
    #[token("true")]
    TokTrue,
    #[token("until")]
    TokUntil,
    #[token("while")]
    TokWhile,

    #[token("+")]
    TokAdd,
    #[token("-")]
    TokSub,
    #[token("*")]
    TokMul,
    #[token("/")]
    TokDiv,
    #[token("%")]
    TokRem,
    #[token("^")]
    TokPow,
    #[token("#")]
    TokLen,
    #[token("&")]
    TokBAnd,
    #[token("~")]
    TokBNot,
    #[token("|")]
    TokBOr,
    #[token("<<")]
    TokLShift,
    #[token(">>")]
    TokRShift,
    #[token("//")]
    TokIntDiv,
    #[token("==")]
    TokEq,
    #[token("~=")]
    TokNEq,
    #[token("<=")]
    TokLe,
    #[token(">=")]
    TokGe,
    #[token("<")]
    TokLt,
    #[token(">")]
    TokGt,
    #[token("=")]
    TokAssign,
    #[token("(")]
    TokLParen,
    #[token(")")]
    TokRParen,
    #[token("{")]
    TokLBrace,
    #[token("}")]
    TokRBrace,
    #[token("[")]
    TokLBracket,
    #[token("]")]
    TokRBracket,
    #[token("::")]
    TokLabel,
    #[token(";")]
    TokSemicolon,
    #[token(":")]
    TokColon,
    #[token(",")]
    TokComma,
    #[token(".")]
    TokDot,
    #[token("..")]
    TokConcat,
    #[token("...")]
    TokVararg,

    #[regex(r"[a-zA-Z_][a-zA-Z_0-9]*")]
    TokIdent,
    #[regex(r"(\d+\.?\d*|\d*\.?\d+)([eE][-+]?\d+)?")]
    #[regex(r"0[xX]([0-9a-fA-F]+\.?[0-9a-fA-F]*|[0-9a-fA-F]*\.?[0-9a-fA-F]+)([pP][-+]?\d+)?")]
    TokNumber,
    #[regex(r#""(\\"|[^"])*""#)]
    TokString,

    ExprNumber,
    ExprString,
    ExprBoolean,
    ExprNil,
    ExprVararg,
    ExprTable,
    ExprFunction,
    TableEntry,
    TableKey,
    TableValue,
    ExprBinary,
    BinaryOp,
    ExprUnary,
    UnaryOp,
    ExprParens,
    ExprCall,
    Callee,
    Method,
    CallArgs,
    VarName,
    VarField,
    StatementEmpty,
    StatementLocalDecl,
    StatementLocalFuncDecl,
    StatementFuncDecl,
    FuncArgs,
    StatementCall,
    StatementAssign,
    StatementDo,
    StatementWhile,
    StatementRepeat,
    StatementNumericFor,
    StatementGenericFor,
    StatementIf,
    IfBranch,
    ElseIfBranch,
    ElseBranch,
    StatementGoto,
    StatementLabel,
    StatementBreak,
    StatementReturn,
    Block,

    #[error]
    UnrecognizedCharacter,
    Error,
}

impl SyntaxKind {
    pub fn explain_token(self) -> &'static str {
        match self {
            SyntaxKind::TokBreak => "`break`",
            SyntaxKind::TokDo => "`do`",
            SyntaxKind::TokElse => "`else`",
            SyntaxKind::TokElseIf => "`elseif`",
            SyntaxKind::TokEnd => "`end`",
            SyntaxKind::TokFalse => "`false`",
            SyntaxKind::TokFor => "`for`",
            SyntaxKind::TokFunction => "`function`",
            SyntaxKind::TokGoto => "`goto`",
            SyntaxKind::TokIf => "`if`",
            SyntaxKind::TokIn => "`in`",
            SyntaxKind::TokLocal => "`local`",
            SyntaxKind::TokNil => "`nil`",
            SyntaxKind::TokRepeat => "`repeat`",
            SyntaxKind::TokReturn => "`return`",
            SyntaxKind::TokThen => "`then`",
            SyntaxKind::TokTrue => "`true`",
            SyntaxKind::TokUntil => "`until`",
            SyntaxKind::TokWhile => "`while`",
            SyntaxKind::TokAssign => "`=`",
            SyntaxKind::TokLParen => "`(`",
            SyntaxKind::TokRParen => "`)`",
            SyntaxKind::TokLBrace => "`{`",
            SyntaxKind::TokRBrace => "`}`",
            SyntaxKind::TokLBracket => "`[`",
            SyntaxKind::TokRBracket => "`]`",
            SyntaxKind::TokLabel => "`::`",
            SyntaxKind::TokSemicolon => "`;`",
            SyntaxKind::TokColon => "`:`",
            SyntaxKind::TokComma => "`,`",
            SyntaxKind::TokDot => "`.`",
            SyntaxKind::TokVararg => "`...`",
            SyntaxKind::TokIdent => "an identifier",
            SyntaxKind::TokNumber => "a number",
            SyntaxKind::TokString => "a string",
            SyntaxKind::TokAnd => "`and`",
            SyntaxKind::TokNot => "`not`",
            SyntaxKind::TokOr => "`or`",
            SyntaxKind::TokAdd => "`+`",
            SyntaxKind::TokSub => "`-`",
            SyntaxKind::TokMul => "`*`",
            SyntaxKind::TokDiv => "`/`",
            SyntaxKind::TokRem => "`%`",
            SyntaxKind::TokPow => "`^`",
            SyntaxKind::TokLen => "`#`",
            SyntaxKind::TokBAnd => "`&`",
            SyntaxKind::TokBNot => "`~`",
            SyntaxKind::TokBOr => "`|`",
            SyntaxKind::TokLShift => "`<<`",
            SyntaxKind::TokRShift => "`>>`",
            SyntaxKind::TokIntDiv => "`//`",
            SyntaxKind::TokEq => "`==`",
            SyntaxKind::TokNEq => "`~=`",
            SyntaxKind::TokLe => "`<=`",
            SyntaxKind::TokGe => "`>=`",
            SyntaxKind::TokLt => "`<`",
            SyntaxKind::TokGt => "`>`",
            SyntaxKind::TokConcat => "..",
            SyntaxKind::UnrecognizedCharacter => "an unrecognized character",
            _ => "?",
        }
    }

    pub const UNOPS: &'static [SyntaxKind] = &[
        SyntaxKind::TokNot,
        SyntaxKind::TokLen,
        SyntaxKind::TokSub,
        SyntaxKind::TokBNot,
    ];

    pub fn unop_binding_power(self) -> Option<u8> {
        Self::UNOPS.contains(&self).then(|| 21)
    }

    pub fn is_trivia(self) -> bool {
        matches!(self, SyntaxKind::Whitespace | SyntaxKind::Comment)
    }
}

macro_rules! define_binops {
    ($(($name:ident, $bp:expr), )+) => {
        impl SyntaxKind {
            pub const BINOPS: &'static [SyntaxKind] = &[$(SyntaxKind::$name,)+];

            pub fn binop_binding_power(self) -> Option<(u8, u8)> {
                Some(match self {
                    $(SyntaxKind::$name => $bp,)+
                    _ => return None
                })
            }
        }
    }
}

define_binops![
    (TokOr, (1, 2)),
    (TokAnd, (3, 4)),
    (TokLt, (5, 6)),
    (TokGt, (5, 6)),
    (TokLe, (5, 6)),
    (TokGe, (5, 6)),
    (TokNEq, (5, 6)),
    (TokEq, (5, 6)),
    (TokBOr, (7, 8)),
    (TokBNot, (9, 10)),
    (TokBAnd, (11, 12)),
    (TokLShift, (13, 14)),
    (TokRShift, (13, 14)),
    (TokConcat, (16, 15)),
    (TokAdd, (17, 18)),
    (TokSub, (17, 18)),
    (TokMul, (19, 20)),
    (TokDiv, (19, 20)),
    (TokIntDiv, (19, 20)),
    (TokRem, (19, 20)),
    (TokPow, (23, 22)),
];

impl From<SyntaxKind> for rowan::SyntaxKind {
    fn from(v: SyntaxKind) -> Self {
        Self(v as u16)
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Lua {}

impl rowan::Language for Lua {
    type Kind = SyntaxKind;

    fn kind_from_raw(raw: rowan::SyntaxKind) -> Self::Kind {
        assert!(raw.0 <= SyntaxKind::Error as u16);
        unsafe { std::mem::transmute::<u16, SyntaxKind>(raw.0) }
    }

    fn kind_to_raw(kind: Self::Kind) -> rowan::SyntaxKind {
        kind.into()
    }
}
