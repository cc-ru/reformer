use std::collections::{HashMap, HashSet};

use once_cell::sync::Lazy;
use rowan::{Checkpoint, GreenNode, GreenNodeBuilder};

use super::Lexer;
use super::SyntaxKind::{self, *};

pub struct ParseResult {
    pub root: GreenNode,
    pub errors: Vec<String>,
}

pub struct Parser<'s> {
    pos: usize,
    lexer: Lexer<'s>,
    builder: GreenNodeBuilder<'static>,
    errors: Vec<String>,
    recovery_set: HashMap<SyntaxKind, u32>,
    recovery_level: u32,
}

impl Parser<'_> {
    pub fn new(lexer: Lexer<'_>) -> Parser<'_> {
        Parser {
            pos: 0,
            lexer,
            builder: GreenNodeBuilder::new(),
            errors: Vec::new(),
            recovery_set: HashMap::new(),
            recovery_level: 0,
        }
    }

    pub fn parse(mut self) -> ParseResult {
        self.parse_block();

        let root = self.builder.finish();
        ParseResult {
            root,
            errors: self.errors,
        }
    }

    fn checkpoint(&self) -> Checkpoint {
        self.builder.checkpoint()
    }

    fn start_node(&mut self, kind: SyntaxKind) {
        self.builder.start_node(kind.into());
    }

    fn start_node_at(&mut self, checkpoint: Checkpoint, kind: SyntaxKind) {
        self.builder.start_node_at(checkpoint, kind.into());
    }

    fn finish_node(&mut self) {
        self.builder.finish_node();
    }

    fn start_error(&mut self, message: impl Into<String>) {
        self.errors.push(message.into());
        self.start_node(SyntaxKind::Error);
    }

    fn bump(&mut self) -> SyntaxKind {
        let token = self.lexer.next().unwrap();
        let text = self.lexer.slice();
        if token == Error {
            self.errors.push("unexpected character".into());
        }

        self.builder.token(token.into(), text);
        self.pos += 1;

        token
    }

    fn skip_trivia(&mut self) {
        while self.lexer.peek().filter(|t| t.is_trivia()).is_some() {
            self.bump();
        }
    }

    fn peek(&mut self) -> Option<SyntaxKind> {
        self.skip_trivia();
        self.lexer.peek()
    }

    fn push_recovery(&mut self, tokens: &[SyntaxKind]) {
        for &token in tokens {
            self.recovery_set
                .entry(token)
                .or_insert(self.recovery_level);
        }
        self.recovery_level += 1;
    }

    fn pop_recovery(&mut self) {
        self.recovery_level -= 1;
        let level = self.recovery_level;
        self.recovery_set.retain(|_, v| *v < level);
    }

    fn unexpected_token(&mut self, found: Option<SyntaxKind>, mut message: String) {
        message.push_str(" near ");

        message.push_str(match found {
            Some(tok) => tok.explain_token(),
            None => "end of file",
        });

        self.start_error(message);
        while let Some(tok) = self.peek() {
            if self.recovery_set.contains_key(&tok) {
                break;
            } else {
                self.bump();
            }
        }
        self.finish_node();
    }

    fn expect(&mut self, expect: impl Expect) -> SyntaxKind {
        match self.peek() {
            Some(tok) if expect.is_match(tok) => {
                self.bump();
                tok
            }
            found => {
                let message = format_expected_list(&expect.to_vec());
                self.unexpected_token(found, message);
                SyntaxKind::Error
            }
        }
    }

    fn wrap(&mut self, node: SyntaxKind) -> SyntaxKind {
        self.start_node(node);
        let tok = self.bump();
        self.finish_node();
        tok
    }
}

trait Expect: Copy {
    fn is_match(self, tok: SyntaxKind) -> bool;
    fn to_vec(self) -> Vec<SyntaxKind>;
}

impl Expect for SyntaxKind {
    fn is_match(self, tok: SyntaxKind) -> bool {
        self == tok
    }

    fn to_vec(self) -> Vec<SyntaxKind> {
        vec![self]
    }
}

impl Expect for &[SyntaxKind] {
    fn is_match(self, tok: SyntaxKind) -> bool {
        self.contains(&tok)
    }

    fn to_vec(self) -> Vec<SyntaxKind> {
        self.into()
    }
}

macro_rules! lookahead {
    ($s:ident { $( $expect:expr => $expr:expr, )+ }) => {
        lookahead!($s { $( $expect => $expr, )+ @err => (), });
    };

    ($s:ident { $( $expect:expr => $expr:expr, )+ @err => $err:expr, }) => {
        match $s.peek() {
            $( Some(tok) if $expect.is_match(tok) => $expr, )+
            found => {
                let tokens: Vec<_> = vec![$($expect.to_vec(), )+].into_iter().flatten().collect();
                let message = format_expected_list(&tokens);
                $s.unexpected_token(found, message);
                $err
            }
        }
    };

    ($s:ident { $( $expect:expr => $expr:expr, )+ _ => $catch:expr, }) => {
        match $s.peek() {
            $( Some(tok) if $expect.is_match(tok) => $expr, )+
            _ => $catch
        }
    };
}

impl Parser<'_> {
    fn parse_expr(&mut self) {
        self.parse_expr_rec(0);
    }

    #[rustfmt::skip]
    const EXPR_TOKENS: Lazy<Vec<SyntaxKind>> = Lazy::new(|| {
        let mut tokens = vec![
            TokNumber, TokString, TokTrue, TokFalse, TokNil,
            TokVararg, TokLBrace, TokIdent, TokLParen, TokFunction
        ];
        tokens.extend_from_slice(SyntaxKind::UNOPS);
        tokens
    });

    fn parse_expr_rec(&mut self, min_bp: u8) {
        let checkpoint = self.checkpoint();

        lookahead!(self {
            TokNumber => drop(self.wrap(ExprNumber)),
            TokString => drop(self.wrap(ExprString)),
            &[TokTrue, TokFalse] => drop(self.wrap(ExprBoolean)),
            TokNil => drop(self.wrap(ExprNil)),
            TokVararg => drop(self.wrap(ExprVararg)),
            TokLBrace => self.parse_table(),
            &[TokIdent, TokLParen] => self.parse_prefix_expr(),
            TokFunction => {
                self.start_node(ExprFunction);
                self.bump();
                self.parse_func_body();
                self.finish_node();
            },
            &SyntaxKind::UNOPS => {
                self.start_node(ExprUnary);
                let op = self.wrap(UnaryOp);
                let bp = op.unop_binding_power().unwrap();
                self.parse_expr_rec(bp);
                self.finish_node();
            },
        });

        self.parse_expr_rec_right(checkpoint, min_bp);
    }

    fn parse_expr_rec_right(&mut self, checkpoint: Checkpoint, min_bp: u8) {
        while let Some(tok) = self.peek() {
            let (l_bp, r_bp) = match tok.binop_binding_power() {
                Some(v) => v,
                _ => break,
            };

            if l_bp < min_bp {
                break;
            }

            self.start_node_at(checkpoint, ExprBinary);
            self.wrap(BinaryOp);
            self.parse_expr_rec(r_bp);
            self.finish_node();
        }
    }

    fn parse_prefix_expr(&mut self) {
        let checkpoint = self.checkpoint();
        self.parse_prefix_expr_left();
        self.parse_prefix_expr_right(checkpoint);
    }

    fn parse_prefix_expr_left(&mut self) -> SyntaxKind {
        lookahead!(self {
            TokIdent => {
                self.wrap(VarName);
                TokIdent
            },
            TokLParen => {
                self.start_node(ExprParens);
                self.bump();
                self.push_recovery(&[TokRParen]);
                self.parse_expr();
                self.expect(TokRParen);
                self.pop_recovery();
                self.finish_node();
                ExprParens
            },
            @err => Error,
        })
    }

    fn parse_field(&mut self, checkpoint: Checkpoint) {
        lookahead!(self {
            TokLBracket => {
                self.start_node_at(checkpoint, VarField);
                self.bump();
                self.push_recovery(&[TokRBracket]);
                self.parse_expr();
                self.expect(TokRBracket);
                self.pop_recovery();
                self.finish_node();
            },
            TokDot => {
                self.start_node_at(checkpoint, VarField);
                self.bump();
                self.expect(TokIdent);
                self.finish_node();
            },
        });
    }

    fn parse_call_args(&mut self) {
        lookahead!(self {
            TokLParen => {
                self.start_node(CallArgs);
                self.parse_paren_expr_list();
                self.finish_node();
            },
            TokString => {
                self.start_node(CallArgs);
                self.start_node(ExprString);
                self.bump();
                self.finish_node();
                self.finish_node();
            },
            TokLBrace => {
                self.start_node(CallArgs);
                self.parse_table();
                self.finish_node();
            },
        });
    }

    fn parse_prefix_expr_right(&mut self, checkpoint: Checkpoint) -> Option<SyntaxKind> {
        let mut res = None;

        loop {
            lookahead!(self {
                &[TokLBracket, TokDot] => {
                    self.parse_field(checkpoint);
                    res = Some(VarField);
                },
                &[TokLParen, TokString, TokLBrace] => {
                    self.start_node_at(checkpoint, ExprCall);
                    self.start_node_at(checkpoint, Callee);
                    self.finish_node();
                    self.parse_call_args();
                    self.finish_node();
                    res = Some(ExprCall);
                },
                TokColon => {
                    self.start_node_at(checkpoint, ExprCall);
                    self.start_node_at(checkpoint, Callee);
                    self.finish_node();
                    self.bump();
                    self.start_node(Method);
                    self.expect(TokIdent);
                    self.finish_node();
                    self.parse_call_args();
                    self.finish_node();
                    res = Some(ExprCall);
                },
                _ => break,
            });
        }

        res
    }

    fn parse_var(&mut self) {
        let checkpoint = self.checkpoint();
        let mut need_field = self.parse_prefix_expr_left() != TokIdent;

        loop {
            if need_field {
                self.parse_field(checkpoint);
                need_field = false;
            } else {
                match self.parse_prefix_expr_right(checkpoint) {
                    Some(VarField) => need_field = false,
                    None => break,
                    _ => need_field = true,
                }
            }
        }
    }

    fn parse_var_or_call(&mut self) -> bool {
        let checkpoint = self.checkpoint();
        let mut is_var = self.parse_prefix_expr_left() == TokIdent;

        loop {
            match self.parse_prefix_expr_right(checkpoint) {
                Some(VarField) => is_var = true,
                Some(_) => is_var = false,
                _ => break,
            }
        }

        is_var
    }

    fn parse_punctuated(
        &mut self,
        tokens: impl Expect,
        sep: impl Expect,
        mut f: impl FnMut(&mut Self),
    ) {
        lookahead!(self {
            tokens => f(self),
            @err => return,
        });

        loop {
            lookahead!(self {
                sep => {
                    self.bump();
                    lookahead!(self {
                        tokens => f(self),
                    });
                },
                _ => break,
            })
        }
    }

    fn parse_paren_expr_list(&mut self) {
        self.expect(TokLParen);
        self.push_recovery(&[TokRParen]);
        lookahead!(self {
            Self::EXPR_TOKENS => {
                self.parse_punctuated(&Self::EXPR_TOKENS[..], TokComma, |s| s.parse_expr());
                self.expect(TokRParen);
            },
            TokRParen => {
                self.bump();
            },
        });
        self.pop_recovery();
    }

    fn parse_expr_list(&mut self) {
        self.parse_punctuated(&Self::EXPR_TOKENS[..], TokComma, |s| s.parse_expr());
    }

    fn parse_ident_list(&mut self) {
        self.parse_punctuated(TokIdent, TokComma, |s| drop(s.bump()));
    }

    #[rustfmt::skip]
    const TABLE_VAR_TOKENS: Lazy<Vec<SyntaxKind>> = Lazy::new(|| {
        let mut tokens = vec![
            TokLBracket, TokDot, TokLBrace, TokString, TokColon, TokLParen, TokComma, TokRBrace
        ];
        tokens.extend_from_slice(SyntaxKind::BINOPS);
        tokens
    });

    fn parse_table(&mut self) {
        self.start_node(ExprTable);
        self.expect(TokLBrace);
        self.push_recovery(&[TokRBrace, TokComma]);

        while self.peek().is_some() {
            let checkpoint = self.checkpoint();

            let need_value = lookahead!(self {
                TokRBrace => {
                    self.bump();
                    break;
                },

                TokLBracket => {
                    self.push_recovery(&[TokAssign]);
                    self.start_node(TableKey);
                    self.bump();
                    self.push_recovery(&[TokRBracket]);
                    self.parse_expr();
                    self.expect(TokRBracket);
                    self.pop_recovery();
                    self.finish_node();
                    true
                },

                TokIdent => {
                    let checkpoint = self.checkpoint();
                    self.bump();

                    lookahead!(self {
                        TokAssign => {
                            self.start_node_at(checkpoint, TableKey);
                            self.finish_node();
                            self.push_recovery(&[TokAssign]);
                            true
                        },
                        Self::TABLE_VAR_TOKENS => {
                            self.start_node_at(checkpoint, TableEntry);
                            self.start_node_at(checkpoint, TableValue);
                            self.start_node_at(checkpoint, VarName);
                            self.finish_node();
                            self.finish_node();
                            self.parse_prefix_expr_right(checkpoint);
                            self.parse_expr_rec_right(checkpoint, 0);
                            self.finish_node();
                            false
                        },
                        @err => false,
                    })
                },

                Self::EXPR_TOKENS => {
                    self.start_node(TableEntry);
                    self.start_node(TableValue);
                    self.parse_expr();
                    self.finish_node();
                    self.finish_node();
                    false
                },

                @err => false,
            });

            if need_value {
                self.start_node_at(checkpoint, TableEntry);
                self.expect(TokAssign);
                self.pop_recovery();
                self.start_node(TableValue);
                self.parse_expr();
                self.finish_node();
                self.finish_node();
            }

            lookahead!(self {
                TokComma => {
                    self.bump();
                },
                TokRBrace => {
                    self.bump();
                    break;
                },
            });

            if self.peek() == Some(TokComma) {
                self.bump();
            }
        }

        self.pop_recovery();
        self.finish_node();
    }

    #[rustfmt::skip]
    const STATEMENT_TOKENS: &'static [SyntaxKind] = &[
        TokSemicolon, TokLocal, TokIdent, TokLParen, TokWhile, TokFor, TokIf, TokDo, TokRepeat,
        TokGoto, TokLabel, TokBreak, TokReturn, TokFunction,
    ];

    fn parse_statement(&mut self) {
        let checkpoint = self.checkpoint();

        lookahead!(self {
            TokSemicolon => drop(self.wrap(StatementEmpty)),
            TokLocal => self.parse_local_decl(),
            &[TokIdent, TokLParen] => {
                if self.parse_var_or_call() {
                    self.start_node_at(checkpoint, StatementAssign);
                    lookahead!(self {
                        TokAssign => (),
                        TokComma => {
                            self.bump();
                            self.parse_punctuated(&[TokIdent, TokLParen][..], TokComma, |s| s.parse_var());
                        },
                    });
                    self.expect(TokAssign);
                    self.parse_expr_list();
                    self.finish_node();
                } else {
                    self.start_node_at(checkpoint, StatementCall);
                    self.finish_node();
                }
            },
            TokDo => self.parse_do(),
            TokWhile => self.parse_while(),
            TokRepeat => self.parse_repeat(),
            TokFor => self.parse_for(),
            TokIf => self.parse_if(),
            TokGoto => self.parse_goto(),
            TokLabel => self.parse_label(),
            TokBreak => drop(self.wrap(StatementBreak)),
            TokReturn => self.parse_return(),
            TokFunction => self.parse_func_decl(),
        });
    }

    fn parse_local_decl(&mut self) {
        let checkpoint = self.checkpoint();
        self.expect(TokLocal);
        lookahead!(self {
            TokIdent => {
                self.start_node_at(checkpoint, StatementLocalDecl);
                self.parse_ident_list();
                if self.peek() == Some(TokAssign) {
                    self.bump();
                    self.parse_expr_list();
                }
                self.finish_node();
            },
            TokFunction => {
                self.start_node_at(checkpoint, StatementLocalFuncDecl);
                self.bump();
                self.expect(TokIdent);
                self.parse_func_body();
                self.finish_node();
            },
        });
    }

    fn parse_func_decl(&mut self) {
        self.start_node(StatementFuncDecl);
        self.expect(TokFunction);
        self.parse_punctuated(TokIdent, TokDot, |s| drop(s.bump()));
        lookahead!(self {
            TokColon => {
                self.bump();
                self.expect(TokIdent);
            },
            TokLParen => {},
        });
        self.parse_func_body();
        self.finish_node();
    }

    fn parse_block_end(&mut self) {
        self.push_recovery(&[TokEnd]);
        self.parse_block();
        self.expect(TokEnd);
        self.pop_recovery();
    }

    fn parse_do(&mut self) {
        self.start_node(StatementDo);
        self.expect(TokDo);
        self.parse_block_end();
        self.finish_node();
    }

    fn parse_while(&mut self) {
        self.start_node(StatementWhile);
        self.expect(TokWhile);
        self.parse_expr();
        self.expect(TokDo);
        self.parse_block_end();
        self.finish_node();
    }

    fn parse_repeat(&mut self) {
        self.start_node(StatementRepeat);
        self.expect(TokRepeat);
        self.push_recovery(&[TokUntil]);
        self.parse_block();
        self.expect(TokUntil);
        self.pop_recovery();
        self.parse_expr();
        self.finish_node();
    }

    fn parse_for(&mut self) {
        let checkpoint = self.checkpoint();

        self.expect(TokFor);
        self.expect(TokIdent);

        lookahead!(self {
            TokAssign => {
                self.start_node_at(checkpoint, StatementNumericFor);
                self.bump();
                self.parse_expr();
                self.expect(TokComma);
                self.parse_expr();
                lookahead!(self {
                    TokComma => {
                        self.bump();
                        self.parse_expr();
                    },
                    TokDo => {},
                });
                self.expect(TokDo);
                self.parse_block_end();
                self.finish_node();
                return;
            },
            TokComma => {
                self.bump();
                self.parse_ident_list();
            },
            TokIn => (),
        });

        self.start_node_at(checkpoint, StatementGenericFor);
        self.expect(TokIn);
        self.parse_expr_list();
        self.expect(TokDo);
        self.parse_block_end();
        self.finish_node();
    }

    fn parse_if(&mut self) {
        self.start_node(StatementIf);
        self.push_recovery(&[TokElse, TokElseIf, TokEnd]);
        self.parse_if_branch();

        while self.peek().is_some() {
            lookahead!(self {
                TokElseIf => {
                    self.parse_elseif_branch();
                },
                TokElse => {
                    self.pop_recovery();
                    self.push_recovery(&[TokEnd]);
                    self.parse_else_branch();
                    self.expect(TokEnd);
                    break;
                },
                TokEnd => {
                    self.bump();
                    break;
                },
            });
        }

        self.pop_recovery();
        self.finish_node();
    }

    fn parse_if_branch(&mut self) {
        self.start_node(IfBranch);
        self.expect(TokIf);
        self.push_recovery(&[TokThen]);
        self.parse_expr();
        self.expect(TokThen);
        self.pop_recovery();
        self.parse_block();
        self.finish_node();
    }

    fn parse_elseif_branch(&mut self) {
        self.start_node(ElseIfBranch);
        self.expect(TokElseIf);
        self.push_recovery(&[TokThen]);
        self.parse_expr();
        self.expect(TokThen);
        self.pop_recovery();
        self.parse_block();
        self.finish_node();
    }

    fn parse_else_branch(&mut self) {
        self.start_node(ElseBranch);
        self.expect(TokElse);
        self.parse_block();
        self.finish_node();
    }

    fn parse_goto(&mut self) {
        self.start_node(StatementGoto);
        self.expect(TokGoto);
        self.expect(TokIdent);
        self.finish_node();
    }

    fn parse_label(&mut self) {
        self.start_node(StatementLabel);
        self.expect(TokLabel);
        self.expect(TokIdent);
        self.expect(TokLabel);
        self.finish_node();
    }

    fn parse_return(&mut self) {
        self.start_node(StatementReturn);
        self.expect(TokReturn);
        lookahead!(self {
            Self::EXPR_TOKENS => {
                self.parse_expr_list();
            },
            _ => {},
        });
        self.finish_node();
    }

    fn parse_func_body(&mut self) {
        self.start_node(FuncArgs);
        self.expect(TokLParen);

        let mut cont = false;
        lookahead!(self {
            TokVararg => drop(self.bump()),
            TokIdent => {
                self.bump();
                cont = true;
            },
            TokRParen => drop(self.bump()),
        });

        while cont {
            lookahead!(self {
                TokComma => {
                    self.bump();
                    lookahead!(self {
                        TokVararg => {
                            self.bump();
                            self.expect(TokRParen);
                            break;
                        },
                        TokIdent => drop(self.bump()),
                    });
                },
                TokRParen => {
                    self.bump();
                    break;
                },
                @err => break,
            });
        }

        self.finish_node();
        self.parse_block();
        self.expect(TokEnd);
    }

    #[rustfmt::skip]
    const STATEMENT_RECOVERY: &'static [SyntaxKind] = &[
        TokWhile, TokIf, TokFor, TokRepeat, TokReturn, TokBreak, TokLocal, TokFunction,
        TokLabel, TokGoto, TokDo, TokSemicolon, TokIdent,
    ];

    fn parse_block(&mut self) {
        self.start_node(Block);
        self.push_recovery(Self::STATEMENT_RECOVERY);

        while self.peek().is_some() {
            lookahead!(self {
                &[TokEnd, TokUntil, TokElse, TokElseIf] => break,
                _ => self.parse_statement(),
            });
        }

        self.pop_recovery();
        self.finish_node();
    }
}

fn format_expected_list(tokens: &[SyntaxKind]) -> String {
    let mut out = String::from("expected ");

    let mut tokens = tokens.iter().copied().collect::<HashSet<_>>();
    let mut entries = Vec::new();

    if Parser::STATEMENT_TOKENS.iter().all(|v| tokens.contains(&v)) {
        for token in Parser::STATEMENT_TOKENS.iter() {
            tokens.remove(&token);
        }

        entries.push("a statement");
    }

    if Parser::EXPR_TOKENS.iter().all(|v| tokens.contains(&v)) {
        for token in Parser::EXPR_TOKENS.iter() {
            tokens.remove(&token);
        }

        entries.push("an expression");
    }

    let mut has_binops = false;
    if SyntaxKind::BINOPS.iter().all(|v| tokens.contains(&v)) {
        for token in SyntaxKind::BINOPS {
            tokens.remove(&token);
        }

        has_binops = true;
    }

    let mut has_unops = false;
    if SyntaxKind::UNOPS.iter().all(|v| tokens.contains(&v)) {
        for token in SyntaxKind::UNOPS {
            tokens.remove(&token);
        }

        has_unops = true;
    }

    if has_binops && has_unops {
        entries.push("an operator");
    } else if has_binops {
        entries.push("a binary operator");
    } else if has_unops {
        entries.push("an unary operator");
    }

    for token in tokens {
        entries.push(token.explain_token());
    }

    entries.sort();

    for (i, entry) in entries.iter().enumerate() {
        if i > 0 {
            if i == entries.len() - 1 {
                out.push_str(", or ");
            } else {
                out.push_str(", ");
            }
        }

        out.push_str(entry);
    }

    out
}
