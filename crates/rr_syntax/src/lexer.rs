use crate::syntax_kind::SyntaxKind;

pub struct Lexer<'s> {
    logos: logos::Lexer<'s, SyntaxKind>,
    saved: Option<SyntaxKind>,
    slice: &'s str,
}

impl<'s> Lexer<'s> {
    pub fn new(source: &'s str) -> Lexer<'s> {
        Lexer {
            logos: logos::Lexer::new(source),
            saved: None,
            slice: "",
        }
    }

    fn _next(&mut self) -> Option<SyntaxKind> {
        let rem = self.logos.remainder();

        if let Some(len) = scan_long_bracket(rem) {
            self.slice = &rem[..len];
            self.logos.bump(len);
            return Some(SyntaxKind::TokString);
        }

        if rem.starts_with("--") {
            if let Some(len) = scan_long_bracket(&rem[2..]) {
                self.slice = &rem[..2 + len];
                self.logos.bump(2 + len);
                return Some(SyntaxKind::Comment);
            }
        }

        let token = self.logos.next();
        if token.is_some() {
            self.slice = self.logos.slice();
        }

        token
    }

    pub fn next(&mut self) -> Option<SyntaxKind> {
        self.saved.take().or_else(|| self._next())
    }

    pub fn peek(&mut self) -> Option<SyntaxKind> {
        if self.saved.is_none() {
            self.saved = self._next();
        }

        self.saved
    }

    pub fn slice(&self) -> &'s str {
        self.slice
    }
}

fn scan_long_bracket(mut input: &str) -> Option<usize> {
    if !input.starts_with('[') {
        return None;
    }

    input = &input[1..];

    let level = input.chars().take_while(|&c| c == '=').count();

    if !input[level..].starts_with('[') {
        return None;
    }

    input = &input[level + 1..];

    let mut len = 2 + level;
    while !input.is_empty() {
        let pos = match input.find(']') {
            Some(v) => v,
            None => {
                len += input.len();
                break;
            }
        };

        len += pos + 1;
        input = &input[pos + 1..];

        let cur_level = input.chars().take_while(|&c| c == '=').count();
        if cur_level == level {
            if input[level..].starts_with(']') {
                len += level + 1;
                break;
            }
        }
    }

    Some(len)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn simple() {
        let mut lexer = Lexer::new("+-..return");
        assert_eq!(lexer.peek(), Some(SyntaxKind::TokAdd));
        assert_eq!(lexer.slice(), "+");
        assert_eq!(lexer.next(), Some(SyntaxKind::TokAdd));
        assert_eq!(lexer.slice(), "+");
        assert_eq!(lexer.peek(), Some(SyntaxKind::TokSub));
        assert_eq!(lexer.slice(), "-");
        assert_eq!(lexer.next(), Some(SyntaxKind::TokSub));
        assert_eq!(lexer.next(), Some(SyntaxKind::TokConcat));
        assert_eq!(lexer.slice(), "..");
        assert_eq!(lexer.next(), Some(SyntaxKind::TokReturn));
        assert_eq!(lexer.slice(), "return");
        assert_eq!(lexer.peek(), None);
        assert_eq!(lexer.next(), None);
    }

    #[test]
    fn short_comments() {
        let mut lexer = Lexer::new("+--123\n+");
        assert_eq!(lexer.next(), Some(SyntaxKind::TokAdd));
        assert_eq!(lexer.next(), Some(SyntaxKind::Comment));
        assert_eq!(lexer.slice(), "--123");
        assert_eq!(lexer.next(), Some(SyntaxKind::Whitespace));
        assert_eq!(lexer.slice(), "\n");
        assert_eq!(lexer.next(), Some(SyntaxKind::TokAdd));
    }

    #[test]
    fn long_comments() {
        let mut lexer = Lexer::new("+--[[12345]]-");
        assert_eq!(lexer.next(), Some(SyntaxKind::TokAdd));
        assert_eq!(lexer.next(), Some(SyntaxKind::Comment));
        assert_eq!(lexer.slice(), "--[[12345]]");
        assert_eq!(lexer.next(), Some(SyntaxKind::TokSub));

        let mut lexer = Lexer::new("+--[=[12345]=]-");
        assert_eq!(lexer.next(), Some(SyntaxKind::TokAdd));
        assert_eq!(lexer.next(), Some(SyntaxKind::Comment));
        assert_eq!(lexer.slice(), "--[=[12345]=]");
        assert_eq!(lexer.next(), Some(SyntaxKind::TokSub));

        let mut lexer = Lexer::new("+--[=[]]]12345]]]=]-");
        assert_eq!(lexer.next(), Some(SyntaxKind::TokAdd));
        assert_eq!(lexer.next(), Some(SyntaxKind::Comment));
        assert_eq!(lexer.slice(), "--[=[]]]12345]]]=]");
        assert_eq!(lexer.next(), Some(SyntaxKind::TokSub));
    }

    #[test]
    fn string() {
        let mut lexer = Lexer::new(r#"+"123"+"#);
        assert_eq!(lexer.next(), Some(SyntaxKind::TokAdd));
        assert_eq!(lexer.next(), Some(SyntaxKind::TokString));
        assert_eq!(lexer.slice(), r#""123""#);
        assert_eq!(lexer.next(), Some(SyntaxKind::TokAdd));
    }

    #[test]
    fn numbers() {
        let mut lexer = Lexer::new("+123.45+");
        assert_eq!(lexer.next(), Some(SyntaxKind::TokAdd));
        assert_eq!(lexer.next(), Some(SyntaxKind::TokNumber));
        assert_eq!(lexer.slice(), "123.45");
        assert_eq!(lexer.next(), Some(SyntaxKind::TokAdd));

        let mut lexer = Lexer::new("+.45+");
        assert_eq!(lexer.next(), Some(SyntaxKind::TokAdd));
        assert_eq!(lexer.next(), Some(SyntaxKind::TokNumber));
        assert_eq!(lexer.slice(), ".45");
        assert_eq!(lexer.next(), Some(SyntaxKind::TokAdd));

        let mut lexer = Lexer::new("+45.+");
        assert_eq!(lexer.next(), Some(SyntaxKind::TokAdd));
        assert_eq!(lexer.next(), Some(SyntaxKind::TokNumber));
        assert_eq!(lexer.slice(), "45.");
        assert_eq!(lexer.next(), Some(SyntaxKind::TokAdd));

        let mut lexer = Lexer::new("+45.e3+");
        assert_eq!(lexer.next(), Some(SyntaxKind::TokAdd));
        assert_eq!(lexer.next(), Some(SyntaxKind::TokNumber));
        assert_eq!(lexer.slice(), "45.e3");
        assert_eq!(lexer.next(), Some(SyntaxKind::TokAdd));
    }
}
