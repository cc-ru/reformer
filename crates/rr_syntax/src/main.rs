use rowan::SyntaxNode;
use rr_syntax::{Lexer, Lua, Parser};
use rustyline::Editor;

fn main() {
    let mut rl = Editor::<()>::new();
    loop {
        let readline = rl.readline("> ");
        match readline {
            Ok(line) => {
                rl.add_history_entry(line.as_str());
                let lexer = Lexer::new(line.as_str());
                let parser = Parser::new(lexer);
                let res = parser.parse();
                let tree = SyntaxNode::<Lua>::new_root(res.root);
                println!("{:#?}", tree);
                println!("{:?}", res.errors);
            }
            _ => break,
        }
    }
}
