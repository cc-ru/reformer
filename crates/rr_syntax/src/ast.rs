use std::convert::TryFrom;
use std::fmt;

use super::{SyntaxKind, SyntaxNode};

pub trait AstNode {
    fn can_cast(kind: SyntaxKind) -> bool;

    fn cast(syntax: SyntaxNode) -> Option<Self>
    where
        Self: Sized;

    fn syntax(&self) -> &SyntaxNode;
}

macro_rules! define_terms {
    ($( $name:ident, )+) => {
        $(
        #[derive(Clone, Debug, Eq, Hash, PartialEq)]
        pub struct $name {
            syntax: SyntaxNode,
        }

        impl AstNode for $name {
            fn can_cast(kind: SyntaxKind) -> bool {
                kind == SyntaxKind::$name
            }

            fn cast(syntax: SyntaxNode) -> Option<Self>
            where
                Self: Sized,
            {
                if Self::can_cast(syntax.kind()) {
                    Some(Self { syntax })
                } else {
                    None
                }
            }

            fn syntax(&self) -> &SyntaxNode {
                &self.syntax
            }
        }

        impl fmt::Display for $name {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                self.syntax.fmt(f)
            }
        }
        )+
    }
}

#[rustfmt::skip]
define_terms![
    ExprNumber, ExprString, ExprBoolean, ExprNil, ExprVararg, ExprTable,
    ExprFunction, TableEntry, TableKey, TableValue, ExprBinary, ExprUnary,
    ExprParens, ExprCall, Callee, Method, CallArgs, VarName, VarField,
    StatementEmpty, StatementLocalDecl, StatementLocalFuncDecl,
    StatementFuncDecl, FuncArgs, StatementCall, StatementAssign, StatementDo,
    StatementWhile, StatementRepeat, StatementNumericFor, StatementGenericFor,
    StatementIf, IfBranch, ElseIfBranch, ElseBranch, StatementGoto,
    StatementLabel, StatementBreak, StatementReturn, Block, BinaryOp, UnaryOp,
];

macro_rules! define_enum {
    ($name:ident { $( $varname:ident($varnode:ident), )+ }) => {
        #[derive(Clone, Debug, Eq, Hash, PartialEq)]
        pub enum $name {
            $( $varname($varnode), )+
        }

        impl AstNode for $name {
            fn can_cast(kind: SyntaxKind) -> bool {
                match kind {
                    $( SyntaxKind::$varnode => true, )+
                    _ => false
                }
            }

            fn cast(syntax: SyntaxNode) -> Option<Self>
            where
                Self: Sized,
            {
                match syntax.kind() {
                    $( SyntaxKind::$varnode => Some($name::$varname($varnode::cast(syntax).unwrap())), )+
                    _ => None
                }
            }

            fn syntax(&self) -> &SyntaxNode {
                match self {
                    $( $name::$varname(v) => v.syntax(), )+
                }
            }
        }

        impl fmt::Display for $name {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                self.syntax().fmt(f)
            }
        }

        $(
        impl From<$varnode> for $name {
            fn from(v: $varnode) -> $name {
                $name::$varname(v)
            }
        }

        impl TryFrom<$name> for $varnode {
            type Error = ();

            fn try_from(v: $name) -> Result<$varnode, ()> {
                match v {
                    $name::$varname(v) => Ok(v),
                    _ => Err(()),
                }
            }
        }
        )+
    }
}

define_enum!(Expr {
    Number(ExprNumber),
    String(ExprString),
    Boolean(ExprBoolean),
    Nil(ExprNil),
    Vararg(ExprVararg),
    Table(ExprTable),
    Function(ExprFunction),
    Binary(ExprBinary),
    Unary(ExprUnary),
    Parens(ExprParens),
    Call(ExprCall),
    VarName(VarName),
    VarField(VarField),
});

define_enum!(Var {
    Name(VarName),
    Field(VarField),
});

define_enum!(Statement {
    Empty(StatementEmpty),
    LocalDecl(StatementLocalDecl),
    LocalFuncDecl(StatementLocalFuncDecl),
    FuncDecl(StatementFuncDecl),
    Call(StatementCall),
    Assign(StatementAssign),
    Do(StatementDo),
    While(StatementWhile),
    Repeat(StatementRepeat),
    NumericFor(StatementNumericFor),
    GenericFor(StatementGenericFor),
    If(StatementIf),
    Goto(StatementGoto),
    Label(StatementLabel),
    Break(StatementBreak),
    Return(StatementReturn),
});

macro_rules! define_children {
    ($( $parent:ident: $func:ident -> $child:ident, )+) => {
        $(
        impl $parent {
            pub fn $func(&self) -> Option<$child> {
                self.syntax.children().find_map($child::cast)
            }
        }
        )+
    }
}

macro_rules! define_multi_children {
    ($( $parent:ident: $func:ident -> $child:ident, )+) => {
        $(
        impl $parent {
            pub fn $func(&self) -> impl Iterator<Item = $child> + '_ {
                self.syntax.children().filter_map($child::cast)
            }
        }
        )+
    }
}

define_children! {
    ExprFunction: args -> FuncArgs,
    ExprFunction: block -> Block,
    TableEntry: key -> TableKey,
    TableEntry: value -> TableValue,
    ExprBinary: op -> BinaryOp,
    ExprUnary: op -> UnaryOp,
    ExprParens: expr -> Expr,
    ExprCall: callee -> Callee,
    ExprCall: method -> Method,
    ExprCall: args -> CallArgs,
    VarField: table -> Expr,
    VarField: expr_key -> Expr,
    StatementLocalFuncDecl: args -> FuncArgs,
    StatementLocalFuncDecl: block -> Block,
    StatementCall: call -> Expr,
    StatementDo: block -> Block,
    StatementWhile: condition -> Expr,
    StatementWhile: block -> Block,
    StatementRepeat: condition -> Expr,
    StatementRepeat: block -> Block,
    StatementNumericFor: block -> Block,
    StatementGenericFor: block -> Block,
    StatementIf: if_branch -> IfBranch,
    StatementIf: else_branch -> ElseBranch,
    IfBranch: condition -> Expr,
    IfBranch: block -> Block,
    ElseIfBranch: condition -> Expr,
    ElseIfBranch: block -> Block,
    ElseBranch: block -> Block,
}

define_multi_children! {
    ExprTable: entries -> TableEntry,
    CallArgs: exprs -> Expr,
    StatementLocalDecl: exprs -> Expr,
    StatementAssign: vars -> Var,
    StatementAssign: exprs -> Expr,
    StatementGenericFor: exprs -> Expr,
    StatementIf: else_if_branches -> ElseIfBranch,
    StatementReturn: exprs -> Expr,
    Block: statements -> Statement,
}
