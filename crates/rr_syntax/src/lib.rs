mod syntax_kind;
pub use self::syntax_kind::{Lua, SyntaxKind};

mod lexer;
pub use self::lexer::Lexer;

mod parser;
pub use self::parser::Parser;

pub type SyntaxNode = rowan::SyntaxNode<Lua>;
pub type SyntaxToken = rowan::SyntaxToken<Lua>;
pub type SyntaxElement = rowan::NodeOrToken<SyntaxNode, SyntaxToken>;

pub mod ast;
